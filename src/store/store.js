import { configureStore } from "@reduxjs/toolkit";
import ProjectSlice from "./slices/ProjectSlice.js";
import TaskSlice from "./slices/TaskSlice.js";

const store = configureStore({
  reducer: {
    projects: ProjectSlice,
    tasks: TaskSlice,
  },
});

export default store;
