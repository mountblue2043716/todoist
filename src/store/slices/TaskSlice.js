import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import {
  getTasks,
  addTask,
  updateTask,
  deleteTask,
  markTaskCompleted,
  addSubTask,
} from "../../services/API";

const initialState = {
  tasks: [],
};

export const fetchTasks = createAsyncThunk(
  "tasks/fetchTasks",
  async (projectId) => {
    try {
      const response = await getTasks(projectId);
      return response;
    } catch (error) {
      console.error("Error fetching tasks:", error);
    }
  }
);

export const createTask = createAsyncThunk(
  "tasks/addTask",
  async ({ projectId, newTaskName, newTaskDescription }) => {
    try {
      const response = await addTask(
        projectId,
        newTaskName,
        newTaskDescription
      );
      return response;
    } catch (error) {
      console.error("Error adding task:", error);
    }
  }
);

export const editTask = createAsyncThunk(
  "tasks/updateTask",
  async ({ editTaskId, newTaskName, newTaskDescription }) => {
    try {
      const response = await updateTask(
        editTaskId,
        newTaskName,
        newTaskDescription
      );
      return response;
    } catch (error) {
      console.error("Error updating task:", error);
    }
  }
);

export const removeTask = createAsyncThunk(
  "tasks/deleteTask",
  async (taskId) => {
    try {
      await deleteTask(taskId);
      return taskId;
    } catch (error) {
      console.error("Error deleting task:", error);
    }
  }
);

export const setTaskCompleted = createAsyncThunk(
  "tasks/checkTask",
  async (taskId) => {
    try {
      const response = await markTaskCompleted(taskId);
      return taskId;
    } catch (error) {
      console.error("Error marking task as completed:", error);
    }
  }
);

export const setDueDate = createAsyncThunk(
  "tasks/setDate",
  async ({ date, task }) => {
    try {
      const response = await updateTask(
        task?.id,
        task?.name,
        task?.description,
        date
      );
      return response;
    } catch (error) {
      console.error("Error setting due date:", error);
    }
  }
);

export const moveTask = createAsyncThunk(
  "tasks/moveTask",
  async ({ projectId, taskContent, taskDescription, taskId }) => {
    try {
      await addTask(projectId, taskContent, taskDescription);
      await deleteTask(taskId);
      return taskId;
    } catch (error) {
      console.error("Error moving task:", error);
    }
  }
);

export const createSubTask = createAsyncThunk(
  "tasks/addSubTask",
  async ({ newTaskName, newTaskDescription, parentId }) => {
    try {
      const response = await addSubTask(
        newTaskName,
        newTaskDescription,
        parentId
      );
      return response;
    } catch (error) {
      console.error("Error adding sub task:", error);
    }
  }
);

const projectSlice = createSlice({
  name: "task",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchTasks.fulfilled, (state, action) => {
      state.tasks = action.payload;
    });
    builder.addCase(createTask.fulfilled, (state, action) => {
      state.tasks.push(action.payload);
    });
    builder.addCase(editTask.fulfilled, (state, action) => {
      const updatedTask = action.payload;
      state.tasks = state.tasks.map((task) =>
        task.id === updatedTask.id ? updatedTask : task
      );
    });
    builder.addCase(removeTask.fulfilled, (state, action) => {
      const taskId = action.payload;
      state.tasks = state.tasks.filter((task) => task.id !== taskId);
    });
    builder.addCase(setTaskCompleted.fulfilled, (state, action) => {
      const taskId = action.payload;
      state.tasks = state.tasks.filter((task) => task.id !== taskId);
     
    });
    builder.addCase(setDueDate.fulfilled, (state, action) => {
      const updatedTask = action.payload;
      const date = updatedTask?.due.date;
      state.tasks = state.tasks.map((task) => {
        return task?.id === updatedTask?.id
          ? { ...task, due: { ...task.due, date: date } }
          : task;
      });
    });
    builder.addCase(moveTask.fulfilled, (state, action) => {
      const taskId = action.payload;
      state.tasks = state.tasks.filter((task) => task.id !== taskId);
    });
    builder.addCase(createSubTask.fulfilled, (state, action) => {
      state.tasks.push(action.payload);
    });
  },
});

export default projectSlice.reducer;
