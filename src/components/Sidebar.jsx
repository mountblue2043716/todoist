import React, { useState, useEffect } from "react";
import {
  Accordion,
  AccordionSummary,
  Typography,
  Drawer,
  Box,
} from "@mui/material";
import { Toolbar } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Stack from "@mui/material/Stack";
import SnackbarComponent from "./SnackBarComponent";
import { useDispatch, useSelector } from "react-redux";

import CreateAndEditProjectForm from "./CreateAndEditProjectForm";
import ProjectItem from "./ProjectItem";
import { getAllProjects } from "../services/API";
import { setProjects } from "../store/slices/ProjectSlice";

const drawerWidth = 400;

const Sidebar = () => {
  const projects = useSelector((state) => state.projects);
  const dispatch = useDispatch();
  const [newProjectName, setNewProjectName] = useState("");
  const [isFavorite, setIsFavorite] = useState(false);
  const [editProjectId, setEditProjectId] = useState("");
  const [open, setOpen] = useState(false);
  const [color, setColor] = useState("teal");
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    fetchProjects();
  }, []);

  const handleOpen = (e) => {
    setOpen(true);
    e.stopPropagation();
  };

  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  const fetchProjects = () => {
    getAllProjects()
      .then((response) => {
        dispatch(setProjects(response));
      })
      .catch((error) => {
        console.error("Error:", error);
        setErrorMessage("Failed to fetch projects.");
        setSnackbarOpen(true);
      });
  };


  const handleEditProject = (projectToEdit, e) => {
    e.stopPropagation();
    e.preventDefault();
    setEditProjectId(projectToEdit.id);
    setNewProjectName(projectToEdit.name);
    setIsFavorite(projectToEdit.is_favorite);
    setColor(projectToEdit.color);
    handleOpen(e);
  };


  return (
    <Drawer
      variant="permanent"
      sx={{
        width: drawerWidth,
        flexShrink: 0,
        [`& .MuiDrawer-paper`]: {
          width: drawerWidth,
          boxSizing: "border-box",
          zIndex: (theme) => theme.zIndex.appBar - 1,
        },
      }}
    >
      <Toolbar />
      <Box
        bgcolor="#F5F5F5"
        height="100%"
        sx={{ overflow: "auto", padding: "20px" }}
      >
        <Accordion
          sx={{
            marginTop: "20px",
            marginBottom: "20px",
            backgroundColor: "#F5F5F5",
          }}
        >
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            <Typography variant="h6">Favourites</Typography>
          </AccordionSummary>
          {projects
            ?.filter((item) => item.is_favorite)
            .map((project) => (
              <ProjectItem
                key={project.id}
                project={project}
                handleEditProject={handleEditProject}
                hasRemove={true}
              />
            ))}
        </Accordion>
        <Accordion sx={{ backgroundColor: "#F5F5F5" }}>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            <Stack
              direction="row"
              justifyContent="space-between"
              width="95%"
              alignItems="center"
            >
              <Typography variant="h6">Projects</Typography>
              <AddIcon onClick={(e) => handleOpen(e)} />
            </Stack>
          </AccordionSummary>
          {projects
            ?.filter((project) => project.name !== "Inbox")
            .map((project) => (
              <ProjectItem
                key={project.id}
                project={project}
                handleEditProject={handleEditProject}
                hasDelete={true}
                hasFavorite={true}
              />
            ))}
        </Accordion>
      </Box>
      <CreateAndEditProjectForm
        setNewProjectName={setNewProjectName}
        setIsFavorite={setIsFavorite}
        newProjectName={newProjectName}
        isFavorite={isFavorite}
        open={open}
        editProjectId={editProjectId}
        color={color}
        setColor={setColor}
        setEditProjectId={setEditProjectId}
        setOpen={setOpen}
      />
      <SnackbarComponent
        open={snackbarOpen}
        message={errorMessage}
        onClose={handleSnackbarClose}
      />
    </Drawer>
  );
};

export default Sidebar;
