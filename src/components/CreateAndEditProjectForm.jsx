import React, { useState } from "react";
import MySwitch from "./Switch";
import { TextField, Modal, Box, Button } from "@mui/material";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { useDispatch } from "react-redux";

import { createNewProject, updateProject } from "../services/API";
import SnackbarComponent from "./SnackBarComponent";
import { editProject, createProject } from "../store/slices/ProjectSlice";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const colors = [
  { id: 31, name: "red" },
  { id: 32, name: "orange" },
  { id: 33, name: "yellow" },
  { id: 36, name: "green" },
  { id: 38, name: "teal" },
  { id: 41, name: "blue" },
  { id: 43, name: "violet" },
  { id: 44, name: "lavender" },
  { id: 45, name: "magenta" },
  { id: 46, name: "salmon" },
  { id: 48, name: "grey" },
];

const CreateAndEditProjectForm = ({
  setNewProjectName,
  setIsFavorite,
  newProjectName,
  isFavorite,
  open,
  editProjectId,
  color,
  setColor,
  setEditProjectId,
  setOpen,
}) => {
  const dispatch = useDispatch();
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const handleChange = (event) => {
    setColor(event.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (editProjectId) {
      updateProject(editProjectId, newProjectName, isFavorite, color)
        .then((response) => {
          dispatch(editProject(response));
          handleClose();
        })
        .catch((error) => {
          handleClose();
          console.error("Error updating project:", error);
          setErrorMessage("Failed updating project.");
          setSnackbarOpen(true);
        });
    } else {
      createNewProject(newProjectName, isFavorite, color)
        .then((response) => {
          dispatch(createProject(response));
          handleClose();
        })
        .catch((error) => {
          handleClose();
          console.error("Error creating project:", error);
          setErrorMessage("Failed creating project.");
          setSnackbarOpen(true);
        });
    }
  };

  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  const handleClose = () => {
    setOpen(!open);
    setEditProjectId("");
    setNewProjectName("");
    setIsFavorite(false);
    setColor("teal");
  };

  return (
    <>
      <Modal open={open} onClose={handleClose}>
        <Box sx={style}>
          <h3>Add project</h3>
          <form onSubmit={(e) => handleSubmit(e)}>
            <TextField
              label="Project Name"
              size="small"
              value={newProjectName}
              onChange={(e) => setNewProjectName(e.target.value)}
              fullWidth
              required
            />
            <MySwitch setIsFavorite={setIsFavorite} isFavorite={isFavorite} />
            Add to favorites
            <br />
            <Box
              sx={{ minWidth: 120, marginTop: "20px", marginBottom: "20px" }}
            >
              <FormControl fullWidth>
                <InputLabel id="color">Color</InputLabel>
                <Select
                  labelId="color"
                  id="color"
                  value={color}
                  label="color"
                  size="small"
                  onChange={handleChange}
                >
                  {colors.map((color) => (
                    <MenuItem key={color.id} value={color.name}>
                      <div
                        style={{
                          width: "10px",
                          height: "10px",
                          backgroundColor: color.name,
                          borderRadius: "50%",
                          marginRight: "7px",
                        }}
                      ></div>
                      {color.name}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>
            <div
              style={{
                display: "flex",
                justifyContent: "flex-end",
                columnGap: "10px",
                marginTop: "10px",
              }}
            >
              <Button variant="outlined" onClick={handleClose}>
                Cancel
              </Button>
              <Button type="submit" variant="contained" color="primary">
                {editProjectId ? "Save" : "Add "}
              </Button>
            </div>
          </form>
        </Box>
      </Modal>
      <SnackbarComponent
        open={snackbarOpen}
        message={errorMessage}
        onClose={handleSnackbarClose}
      />
    </>
  );
};

export default CreateAndEditProjectForm;
