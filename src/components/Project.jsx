import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { List, Button, Box, Collapse } from "@mui/material";
import CircularProgress from "@mui/material/CircularProgress";

import CreateAndEditTaskForm from "./CreateAndEditTaskForm";
import Task from "./Task";
import { useSelector, useDispatch } from "react-redux";
import { fetchTasks, createTask, editTask } from "../store/slices/TaskSlice";

const Project = () => {
  const tasks = useSelector((state) => state.tasks.tasks);
  const projects = useSelector((state) => state.projects);
  const dispatch = useDispatch();

  const { projectId } = useParams();
  const [newTaskName, setNewTaskName] = useState("");
  const [newTaskDescription, setNewTaskDescription] = useState("");
  const [editTaskId, setEditTaskId] = useState("");
  const [expanded, setExpanded] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    dispatch(fetchTasks(projectId))
      .then(() => {
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
      });
  }, [projectId, dispatch]);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (editTaskId) {
      dispatch(editTask({ editTaskId, newTaskName, newTaskDescription }));
      setEditTaskId("");
      setNewTaskName("");
      setNewTaskDescription("");
    } else {
      dispatch(createTask({ projectId, newTaskName, newTaskDescription }));
      setNewTaskName("");
      setNewTaskDescription("");
      setExpanded(false);
    }
  };

  const expandCollapse = (booleanValue) => {
    setExpanded(booleanValue);
    setEditTaskId("");
    setNewTaskName("");
    setNewTaskDescription("");
  };
  const project = projects.find((project) => project.id === projectId);

  return (
    <Box sx={{ padding: "100px", paddingLeft: "200px" }}>
      <h2>{project && project.name}</h2>
      {loading ? (
        <CircularProgress />
      ) : (
        <>
          <List sx={{ width: "40vw" }}>
            {tasks
              ?.filter((task) => task?.parent_id === null)
              ?.map((task) => (
                <Task
                  key={task?.id}
                  task={task}
                  editTaskId={editTaskId}
                  projects={projects}
                  projectId={projectId}
                  tasks={tasks}
                  setEditTaskId={setEditTaskId}
                  handleSubmit={handleSubmit}
                  newTaskName={newTaskName}
                  newTaskDescription={newTaskDescription}
                  expandCollapse={expandCollapse}
                  setExpanded={setExpanded}
                  setNewTaskName={setNewTaskName}
                  setNewTaskDescription={setNewTaskDescription}
                  project={project}
                />
              ))}
          </List>
          <Box>
            <div style={{ marginTop: "10px" }}>
              <Button onClick={() => expandCollapse(true)}>
                {expanded ? "" : "+ Add task"}
              </Button>
              <Collapse in={expanded}>
                <CreateAndEditTaskForm
                  handleSubmit={handleSubmit}
                  newTaskName={newTaskName}
                  setNewTaskName={setNewTaskName}
                  newTaskDescription={newTaskDescription}
                  setNewTaskDescription={setNewTaskDescription}
                  expandCollapse={expandCollapse}
                  submitButtonText="Add task"
                />
              </Collapse>
            </div>
          </Box>
        </>
      )}
    </Box>
  );
};

export default Project;
