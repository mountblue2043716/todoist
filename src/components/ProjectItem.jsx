import React, { useState } from "react";
import { Link } from "react-router-dom";
import { AccordionDetails } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import RemoveCircleIcon from "@mui/icons-material/RemoveCircle";
import DeleteIcon from "@mui/icons-material/Delete";
import FavoriteIcon from "@mui/icons-material/Favorite";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import { useDispatch } from "react-redux";

import AlertDialog from "./DeleteConfirmation";
import { deleteProject } from "../services/API";
import SnackbarComponent from "./SnackBarComponent";
import { removeProject, setFavorite } from "../store/slices/ProjectSlice";
import { updateProject } from "../services/API";

const ProjectItem = ({
  project,
  hasFavorite,
  hasDelete,
  hasRemove,
  handleEditProject,
}) => {
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const handleClickOpen = (e) => {
    e.stopPropagation();
    e.preventDefault();
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  const handleDeleteProject = async (projectId, e) => {
    handleClose();
    deleteProject(projectId)
      .then((response) => {
        dispatch(removeProject(projectId));
      })
      .catch((error) => {
        console.error("Error deleting project:", error);
        setErrorMessage("Failed to delete project.");
        setSnackbarOpen(true);
      });
  };


  const handleToggleFavorite = (project, e) => {
    e.stopPropagation();
    e.preventDefault();
    updateProject(
      project.id,
      project.name,
      !project.is_favorite,
      project.color
    );
    dispatch(setFavorite(project));
  };

  return (
    <>
      <div className="project-item">
        <Link to={`/project/${project.id}`} key={project.id}>
          <AccordionDetails
            key={project.id}
            sx={{ display: "flex", justifyContent: "space-between" }}
          >
            <div style={{ display: "flex", alignItems: "center", gap: "10px" }}>
              <div
                style={{
                  width: "10px",
                  height: "10px",
                  backgroundColor: project.color,
                  borderRadius: "50%",
                }}
              ></div>
              {project.name}
            </div>
            <div className="hover-content">
              {hasRemove && (
                <RemoveCircleIcon
                  onClick={(e) => handleToggleFavorite(project, e)}
                  fontSize="small"
                  sx={{
                    "&:hover": {
                      transform: "scale(1.5)",
                    },
                  }}
                />
              )}
              {hasFavorite &&
                (project.is_favorite ? (
                  <FavoriteIcon
                    fontSize="small"
                    sx={{
                      "&:hover": {
                        transform: "scale(1.5)",
                      },
                    }}
                    onClick={(e) => handleToggleFavorite(project, e)}
                  />
                ) : (
                  <FavoriteBorderIcon
                    fontSize="small"
                    sx={{
                      "&:hover": {
                        transform: "scale(1.5)",
                      },
                    }}
                    onClick={(e) => handleToggleFavorite(project, e)}
                  />
                ))}
              <EditIcon
                fontSize="small"
                onClick={(e) => handleEditProject(project, e)}
                sx={{
                  "&:hover": {
                    transform: "scale(1.5)",
                  },
                  marginLeft: "20px",
                }}
              />
              {hasDelete && (
                <DeleteIcon
                  fontSize="small"
                  sx={{
                    "&:hover": {
                      transform: "scale(1.5)",
                    },
                    marginLeft: "20px",
                  }}
                  onClick={(e) => handleClickOpen(e)}
                />
              )}
            </div>
          </AccordionDetails>
        </Link>
      </div>

      <AlertDialog
        handleDelete={handleDeleteProject}
        open={open}
        handleClose={handleClose}
        projectId={project.id}
        name={project.name}
        type="project"
      />
      <SnackbarComponent
        open={snackbarOpen}
        message={errorMessage}
        onClose={handleSnackbarClose}
      />
    </>
  );
};

export default ProjectItem;
