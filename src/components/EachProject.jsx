import React from "react";
import { MenuItem, Stack } from "@mui/material";
import { useDispatch } from "react-redux";

import { moveTask } from "../store/slices/TaskSlice";

const EachProject = ({
  project,
  taskId,
  taskContent,
  taskDescription,
  handleMoveMenuClose,
  handleMenuClose,
}) => {
  const dispatch = useDispatch();

  const handleMove = (taskId, taskContent, taskDescription, projectId) => {
    handleMoveMenuClose();
    handleMenuClose();
    dispatch(moveTask({ projectId, taskContent, taskDescription, taskId }));
  };

  return (
    <>
      <MenuItem
        onClick={(e) =>
          handleMove(taskId, taskContent, taskDescription, project.id)
        }
      >
        <Stack direction="row" alignItems="center" gap="7px">
          <div
            style={{
              width: "10px",
              height: "10px",
              borderRadius: "50%",
              backgroundColor: project && project.color,
            }}
          ></div>
          <div>{project && project.name}</div>
        </Stack>
      </MenuItem>
    </>
  );
};

export default EachProject;
