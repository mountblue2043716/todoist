import React, { useState } from "react";
import { Modal, Box, Stack, Typography } from "@mui/material";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import { List, Button, Collapse } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";

import CreateAndEditTaskForm from "./CreateAndEditTaskForm";
import SubTask from "./SubTask";
import { createSubTask, editTask } from "../store/slices/TaskSlice";

const TaskModal = ({ open, onClose, task, project, tasks }) => {
  const dispatch = useDispatch();
  const [newTaskName, setNewTaskName] = useState("");
  const [newTaskDescription, setNewTaskDescription] = useState("");
  const [editTaskId, setEditTaskId] = useState("");
  const [expanded, setExpanded] = useState(false);
  const parentId = task?.id;

  const handleSubmit = (e) => {
    e.preventDefault();
    if (editTaskId) {
      dispatch(editTask({ editTaskId, newTaskName, newTaskDescription }));
      setEditTaskId("");
      setNewTaskName("");
      setNewTaskDescription("");
    } else {
      dispatch(createSubTask({ newTaskName, newTaskDescription, parentId }));
      setNewTaskName("");
      setNewTaskDescription("");
      setExpanded(false);
    }
  };

  const expandCollapse = (booleanValue) => {
    setExpanded(booleanValue);
    setEditTaskId("");
    setNewTaskName("");
    setNewTaskDescription("");
  };

  return (
    <Modal open={open} onClose={onClose}>
      <Box
        sx={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          bgcolor: "background.paper",
          boxShadow: 24,
          p: 4,
          width: "50%",
          height: "90%",
          overflow: "scroll",
        }}
      >
        <Stack direction="row" alignItems="center" gap="7px">
          <div
            style={{
              width: "10px",
              height: "10px",
              borderRadius: "50%",
              backgroundColor: project && project.color,
            }}
          ></div>
          <div>{project && project.name}</div>
        </Stack>
        <h2>{task?.content}</h2>
        <p>{task?.description}</p>
        <div>
          {task?.due && (
            <Typography color="gray">
              <CalendarMonthIcon fontSize="14px" sx={{ marginRight: "4px" }} />
              {task?.due?.date}
            </Typography>
          )}
        </div>
        <Typography variant="h6" sx={{ marginTop: "20px" }}>
          Sub-tasks
        </Typography>
        <List>
          {tasks
            ?.filter(
              (subTask) =>
                subTask?.parent_id === task.id && !subTask?.is_completed
            )
            ?.map((subTask) => (
              <SubTask
                key={subTask.id}
                subTask={subTask}
                setEditTaskId={setEditTaskId}
                editTaskId={editTaskId}
                setExpanded={setExpanded}
                handleSubmit={handleSubmit}
                newTaskName={newTaskName}
                newTaskDescription={newTaskDescription}
                setNewTaskName={setNewTaskName}
                setNewTaskDescription={setNewTaskDescription}
                expandCollapse={expandCollapse}
                projectId={project.id}
              />
            ))}
        </List>
        <div style={{ marginTop: "10px" }}>
          <Button onClick={() => expandCollapse(true)}>
            {expanded ? "" : "+ Add sub-task"}
          </Button>
          <Collapse in={expanded}>
            <CreateAndEditTaskForm
              handleSubmit={handleSubmit}
              newTaskName={newTaskName}
              setNewTaskName={setNewTaskName}
              newTaskDescription={newTaskDescription}
              setNewTaskDescription={setNewTaskDescription}
              expandCollapse={expandCollapse}
              submitButtonText="Add task"
            />
          </Collapse>
        </div>
      </Box>
    </Modal>
  );
};

export default TaskModal;
