import React from "react";
import { TextField, Button } from "@mui/material";

const CreateAndEditTaskForm = ({
  handleSubmit,
  newTaskName,
  setNewTaskName,
  newTaskDescription,
  setNewTaskDescription,
  expandCollapse,
  submitButtonText,
}) => {
  return (
    <form onSubmit={(e) => handleSubmit(e)} className="task-form">
      <TextField
        sx={{ width: "50%" }}
        size="small"
        variant="standard"
        label="Task name"
        value={newTaskName}
        onChange={(e) => setNewTaskName(e.target.value)}
        required
      />
      <br />
      <TextField
        sx={{ marginBottom: "10px", marginTop: "6px", width: "50%" }}
        size="small"
        variant="standard"
        label="Description"
        value={newTaskDescription}
        onChange={(e) => setNewTaskDescription(e.target.value)}
      />
      <br />
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <Button
          size="small"
          variant="outlined"
          onClick={() => expandCollapse(false)}
        >
          Cancel
        </Button>
        <Button
          size="small"
          type="submit"
          variant="contained"
          sx={{ marginLeft: "8px" }}
        >
          {submitButtonText}
        </Button>
      </div>
    </form>
  );
};

export default CreateAndEditTaskForm;
