import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  ListItem,
  Checkbox,
  Button,
  Menu,
  MenuItem,
  Stack,
  Typography,
} from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import DriveFileMoveIcon from "@mui/icons-material/DriveFileMove";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import dayjs from "dayjs";
import { DemoContainer, DemoItem } from "@mui/x-date-pickers/internals/demo";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DateCalendar } from "@mui/x-date-pickers/DateCalendar";

import EachProject from "./EachProject";
import AlertDialog from "./DeleteConfirmation";
import CreateAndEditTaskForm from "./CreateAndEditTaskForm";
import {
  removeTask,
  setTaskCompleted,
  setDueDate,
} from "../store/slices/TaskSlice";

const SubTask = ({
  subTask,
  setEditTaskId,
  editTaskId,
  handleSubmit,
  newTaskName,
  setNewTaskName,
  newTaskDescription,
  setNewTaskDescription,
  expandCollapse,
  setExpanded,
  projectId,
}) => {
  const projects = useSelector((state) => state.projects);
  const [openConfirmation, setOpenConfirmation] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [nestedAnchorEl, setNestedAnchorEl] = useState(null);
  const [dateAnchorEl, setDateAnchorEl] = useState(null);
  const dispatch = useDispatch();

  let date = subTask?.due?.date;
  const [newDate, setNewDate] = useState(dayjs(date));
  const handleOpenConfirmation = (e) => {
    e.stopPropagation();
    e.preventDefault();
    setOpenConfirmation(true);
    handleMenuClose();
  };
  const handleCloseConfirmation = () => {
    setOpenConfirmation(false);
  };

  const handleTaskCheck = (taskId) => {
    dispatch(setTaskCompleted(taskId));
  };

  const handleEditTask = (task) => {
    setEditTaskId(task?.id);
    setExpanded(false);
    setNewTaskName(task?.content);
    setNewTaskDescription(task?.description);
    handleMenuClose();
  };

  const handleDeleteTask = async (taskId) => {
    dispatch(removeTask(taskId));
  };

  const handleSetDate = (date) => {
    handleDateMenuClose();
    handleMenuClose();
    setNewDate(date);
    const task = subTask;
    dispatch(setDueDate({ date, task }));
  };

  const handleMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleMenuClose = () => {
    setAnchorEl(null);
  };
  const handleMoveMenuOpen = (event) => {
    setNestedAnchorEl(event.currentTarget);
  };
  const handleMoveMenuClose = () => {
    setNestedAnchorEl(null);
  };
  const handleDateMenuOpen = (event) => {
    setDateAnchorEl(event.currentTarget);
  };
  const handleDateMenuClose = () => {
    setDateAnchorEl(null);
  };

  return (
    <div>
      <ListItem>
        {editTaskId === subTask?.id ? (
          <CreateAndEditTaskForm
            handleSubmit={handleSubmit}
            newTaskName={newTaskName}
            setNewTaskName={setNewTaskName}
            newTaskDescription={newTaskDescription}
            setNewTaskDescription={setNewTaskDescription}
            expandCollapse={expandCollapse}
            submitButtonText="Save"
          />
        ) : (
          <>
            <Checkbox
              onChange={() => handleTaskCheck(subTask?.id)}
              inputProps={{ "aria-label": "checkbox" }}
            />
            <Stack width="100%">
              <Typography fontSize="18px">{subTask?.content}</Typography>
              <Typography color="gray">{subTask?.description}</Typography>
              {subTask?.due && (
                <Typography color="gray">
                  <CalendarMonthIcon
                    fontSize="14px"
                    sx={{ marginRight: "4px" }}
                  />
                  {subTask?.due?.date}
                </Typography>
              )}
              <div>
                <hr style={{ borderColor: "rgba(220, 214, 208, 0.2)" }} />
              </div>
            </Stack>
            <div>
              <Button onClick={handleMenuOpen}>
                <MoreHorizIcon />
              </Button>
              <Menu
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={handleMenuClose}
                MenuListProps={{
                  "aria-labelledby": "basic-button",
                }}
              >
                <MenuItem onClick={() => handleEditTask(subTask)}>
                  <EditIcon fontSize="small" sx={{ marginRight: "10px" }} />
                  Edit task
                </MenuItem>
                <MenuItem onClick={handleDateMenuOpen}>
                  <CalendarMonthIcon
                    fontSize="small"
                    sx={{ marginRight: "10px" }}
                  />
                  Set due date
                </MenuItem>
                <MenuItem onClick={handleMoveMenuOpen}>
                  <DriveFileMoveIcon
                    fontSize="small"
                    sx={{ marginRight: "10px" }}
                  />
                  Move task
                </MenuItem>
                <MenuItem onClick={(e) => handleOpenConfirmation(e)}>
                  <DeleteIcon fontSize="small" sx={{ marginRight: "10px" }} />
                  Delete task
                </MenuItem>
              </Menu>
              <Menu
                anchorEl={nestedAnchorEl}
                open={Boolean(nestedAnchorEl)}
                onClose={handleMoveMenuClose}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                transformOrigin={{
                  vertical: "top",
                  horizontal: "left",
                }}
              >
                {projects
                  .filter(
                    (project) =>
                      project.id !== projectId && project.name !== "Inbox"
                  )
                  .map((project) => (
                    <EachProject
                      key={project.id}
                      project={project}
                      taskId={subTask?.id}
                      taskContent={subTask?.content}
                      taskDescription={subTask?.description}
                      handleMoveMenuClose={handleMoveMenuClose}
                      handleMenuClose={handleMenuClose}
                    />
                  ))}
              </Menu>
              <Menu
                anchorEl={dateAnchorEl}
                open={Boolean(dateAnchorEl)}
                onClose={handleDateMenuClose}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                transformOrigin={{
                  vertical: "top",
                  horizontal: "left",
                }}
              >
                <MenuItem>
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DemoContainer components={["DateCalendar"]}>
                      <DemoItem>
                        <DateCalendar
                          value={newDate}
                          onChange={(date) => handleSetDate(date)}
                        />
                      </DemoItem>
                    </DemoContainer>
                  </LocalizationProvider>
                </MenuItem>
              </Menu>
            </div>
          </>
        )}
      </ListItem>
      <AlertDialog
        handleDelete={handleDeleteTask}
        open={openConfirmation}
        handleClose={handleCloseConfirmation}
        taskId={subTask?.id}
        name={subTask?.content}
        type="task"
      />
    </div>
  );
};

export default SubTask;
