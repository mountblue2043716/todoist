import React from "react";
import { Route, Routes } from "react-router-dom";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { Provider } from "react-redux";

import Sidebar from "./components/Sidebar";
import NavBar from "./components/NavBar";
import Project from "./components/Project";
import Home from "./components/Home";
import store from "./store/store.js";

const theme = createTheme({
  palette: {
    primary: {
      main: "#DB4C3F",
    },
  },
});

const App = () => {
  return (
    <>
      <ThemeProvider theme={theme}>
        <NavBar />
        <Provider store={store}>
          <div className="main-container">
            <Sidebar />
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/project/:projectId" element={<Project />} />
            </Routes>
          </div>
        </Provider>
      </ThemeProvider>
    </>
  );
};

export default App;
