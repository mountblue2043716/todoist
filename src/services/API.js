import { TOKEN } from "../config";
import axios from "axios";
import { v4 as uuidv4 } from "uuid";

axios.defaults.headers.common["Authorization"] = `Bearer ${TOKEN}`;
axios.defaults.headers.common["Content-Type"] = "application/json";
axios.defaults.headers.common["X-Request-Id"] = uuidv4();
axios.defaults.baseURL = "https://api.todoist.com/rest/v2/";

export async function getAllProjects() {
  const response = await axios.get("projects");
  return response.data;
}

export async function createNewProject(
  newProjectName,
  isFavorite,
  color = "teal"
) {
  const newProject = await axios.post("projects", {
    name: newProjectName,
    is_favorite: isFavorite,
    color: color,
  });
  return newProject.data;
}

export async function getTasks(projectId) {
  const response = await axios.get(`tasks?project_id=${projectId}`);
  return response.data;
}

export async function addTask(projectId, taskName, description) {
  const response = await axios.post("tasks", {
    content: taskName,
    project_id: projectId,
    description: description,
  });
  return response.data;
}

export async function markTaskCompleted(taskId) {
  const response = await axios.post(`tasks/${taskId}/close`, {});
  return response.data;
}

export async function updateTask(
  taskId,
  taskName,
  description,
  date = undefined
) {
  const response = await axios.post(`tasks/${taskId}`, {
    content: taskName,
    description: description,
    due_date: date,
  });
  return response.data;
}

export async function deleteProject(projectId) {
  const response = await axios.delete(`projects/${projectId}`);
  return response.data;
}

export async function updateProject(
  projectId,
  projectName,
  isFavorite,
  color = "teal"
) {
  const response = await axios.post(`projects/${projectId}`, {
    name: projectName,
    is_favorite: isFavorite,
    color: color,
  });
  return response.data;
}

export async function deleteTask(taskId) {
  const response = await axios.delete(`tasks/${taskId}`);
  return response.data;
}

export async function getAllTasks() {
  const response = await axios.get(`tasks`);
  return response.data;
}

export async function addSubTask(taskName, description, parentId) {
  const response = await axios.post("tasks", {
    content: taskName,
    description: description,
    parent_id: parentId,
  });
  return response.data;
}


